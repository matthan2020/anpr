from collections import namedtuple
from skimage.filters import threshold_local
from skimage import segmentation
from skimage import measure
from imutils import perspective
import numpy as np
import imutils
import cv2
import time

def load_svm_model(path_svm_model):
    model_svm = cv2.ml.SVM_load(path_svm_model)
    return model_svm

def ocr_plate(plate, model_svm):
    # plate = perspective.four_point_transform(image, region)
    # cv2.imshow("Perspective Transform", imutils.resize(plate, width=400))
    # extract the Value component from the HSV color space and apply adaptive thresholding
    # to reveal the characters on the license plate
    V = cv2.split(cv2.cvtColor(plate, cv2.COLOR_BGR2HSV))[2]
    T = threshold_local(V, 15, offset=10, method="gaussian")
    thresh = (V > T).astype("uint8") * 255
    thresh = cv2.bitwise_not(thresh)

    # resize the license plate region to a canonical size
    plate = imutils.resize(plate, width=400)
    thresh = imutils.resize(thresh, width=400)
    thresh = cv2.medianBlur(thresh, 5)
    # perform a connected components analysis and initialize the mask to store the locations
    # of the character candidates
    labels = measure.label(thresh, connectivity=2, background=0)
    charCandidates = np.zeros(thresh.shape, dtype="uint8")

    # Cau hinh tham so cho model SVM
    digit_w = 30 # Kich thuoc ki tu
    digit_h = 60 # Kich thuoc ki tu
    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue

        # otherwise, construct the label mask to display only connected components for the
        # current label, then find contours in the label mask
        labelMask = np.zeros(thresh.shape, dtype="uint8")
        labelMask[labels == label] = 255
        cnts, _ = cv2.findContours(labelMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        # cnts = cnts[0] if imutils.is_cv2() else cnts[1]
        # ensure at least one contour was found in the mask
        if len(cnts) > 0:
            # grab the largest contour which corresponds to the component in the mask, then
            # grab the bounding box for the contour
            c = max(cnts, key=cv2.contourArea)
            (boxX, boxY, boxW, boxH) = cv2.boundingRect(c)

            # compute the aspect ratio, solidity, and height ratio for the component
            aspectRatio = boxW / float(boxH)
            solidity = cv2.contourArea(c) / float(boxW * boxH)
            heightRatio = boxH / float(plate.shape[0])

            # check to see if the component passes all the tests
            if 0.1 < aspectRatio < 1.0 and solidity > 0.1 and 0.2 < heightRatio < 2.0:
                # compute the convex hull of the contour and draw it on the character
                # candidates mask
                candidate = np.array(labelMask[boxY:boxY + boxH, boxX:boxX + boxW])
                hull = cv2.convexHull(c)
                cv2.drawContours(charCandidates, [hull], -1, 255, -1)
            
    # clear pixels that touch the borders of the character candidates mask and detect
    # contours in the candidates mask

    charCandidates = segmentation.clear_border(charCandidates)
    ctrs, _ = cv2.findContours(charCandidates, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    def license_format(array_candidates):
        first_line = []
        second_line = []
        for candidate, coordinate in array_candidates:
            if coordinate[0] < 60 :
                first_line.append((candidate, coordinate[1]))
            else:
                second_line.append((candidate, coordinate[1]))

        def take_second(s):
            return s[1]

        first_line = sorted(first_line, key=take_second)
        second_line = sorted(second_line, key=take_second)
        
        if len(second_line) == 0: #license plate has one line
            license_plate = "".join([str(ele[0]) for ele in first_line])
        else: #licens plate has two lines
            license_plate = "".join([str(ele[0]) for ele in first_line]) + "-" + "".join([str(ele[0]) for ele in second_line])
        return license_plate

    boxes = []
    char_license = []
    plate_info = ""
    for ctr in ctrs:
        x, y, w, h = cv2.boundingRect(ctr)

        curr_num = thresh[y:y + h, x:x + w]
        curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
        _, curr_num = cv2.threshold(curr_num, 30, 255, cv2.THRESH_BINARY)
        
        curr_num = np.array(curr_num,dtype=np.float32)
        curr_num = curr_num.reshape(-1, digit_w * digit_h)
        

        # Dua vao model SVM
        result = model_svm.predict(curr_num)[1]
        result = int(result[0, 0])

        if result<=9: # Neu la so thi hien thi luon
            result = str(result)
        else: #Neu la chu thi chuyen bang ASCII
            result = chr(result)
        
        char_license.append((result, (y,x)))
        plate_info +=result

    return license_format(char_license)

if __name__ == '__main__':
    plate = cv2.imread('./crop/vuong2.jpg')
    model_svm = load_svm_model('./svm.xml')
    id = ocr_plate(plate, model_svm)
    print(id)