
# python .\yolo_video_detect_licens.py -v ./test_imgs/test_video.MOV -cl ./data/obj.names -w ./backup/yolov4-tiny-obj_best_2.weights -c ./cfg/yolov4-tiny-obj.cfg

# python .\yolo_video_detect_licens.py -v ./test_imgs/test_video.MOV -cl ./data/yolov4-obj.names -w ./backup/yolov4-obj_best.weights -c ./cfg/yolov4-obj.cfg

import time
import cv2
import argparse
import numpy as np
from collections import namedtuple
from skimage.filters import threshold_local
from skimage import segmentation
from skimage import measure
from imutils import perspective
import imutils
import ocr


ap = argparse.ArgumentParser()
ap.add_argument('-v', '--video', required=True,
                help='path to input image', default=0)
ap.add_argument('-c', '--config', required=True,
                help='path to yolo config file')
ap.add_argument('-w', '--weights', required=True,
                help='path to yolo pre-trained weights')
ap.add_argument('-cl', '--classes', required=True,
                help='path to text file containing class names')
args = ap.parse_args()


def get_output_layers(net):
    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers


def draw_prediction(img, id, class_id, confidence, x, y, x_plus_w, y_plus_h):
    label = id
    
    color = COLORS[class_id]

    cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 2)

    cv2.putText(img, label, (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    
model_svm = ocr.load_svm_model('./svm.xml')
# Cap
# cap = cv2.VideoCapture('rtsp://admin:CongNghe@192.168.1.126/streaming/channels/101')
cap = cv2.VideoCapture(args.video)
# class
classes = None

with open(args.classes, 'r') as f:
    classes = [line.strip() for line in f.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(classes), 3))

# Initial YoLov4 model
net = cv2.dnn.readNet(args.weights, args.config)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)


while(True):
    # Capture frame-by-frame
    ret, image_frame = cap.read()
    image = cv2.resize(image_frame, dsize=(720,480))
    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392

    blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0, 0, 0), True, crop=False)

    net.setInput(blob)

    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])

    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]

        plate = image[round(y):round(y + h), round(x):round(x + w)]
        id = ocr.ocr_plate(plate, model_svm)
        if len(id) == 8:
            id_true = id
            print(id_true)
        else:
            continue
        draw_prediction(image, id_true, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h))
        
    cv2.imshow("object detection", image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()