
# importing os module 
import os 
  
# Function to rename multiple files 
def main(): 
    dic = './img_in/'
    for count, filename in enumerate(os.listdir(dic)): 
        dst ="plate" + str(count) + ".jpg"
        src = dic + filename 
        dst = dic + dst 
          
        # rename() function will 
        # rename all the files 
        os.rename(src, dst) 
        print(count)
# Driver Code 
if __name__ == '__main__': 
      
    # Calling main() function 
    main() 