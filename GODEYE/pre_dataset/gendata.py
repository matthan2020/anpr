import cv2
import matplotlib.pyplot as plt
import os
import random
import imutils

# Thêm biên kích thước ngẫu nhiên
def add_boder(image_path, low, high, dir_path, genpath):
    """
    low: kích thước biên thấp nhất (pixel)
    hight: kích thước biên lớn nhất (pixel)
    """
    # random các kích thước biên trong khoảng (low, high)
    top=random.randint(low,high)
    bottom=random.randint(low,high)
    left=random.randint(low,high)
    right=random.randint(low,high)
    
    image=cv2.imread(dir_path + image_path)
    original_width,original_height=image.shape[1],image.shape[0]
    
    #sử dụng hàm của opencv để thêm biên
    image=cv2.copyMakeBorder(image,top,bottom,left,right,cv2.BORDER_REPLICATE)
    
    #sau đó resize ảnh bằng kích thước ban đầu của ảnh
    image=cv2.resize(image,(original_width,original_height))
    cv2.imwrite(genpath + 'boder' + image_path,image)

# Crop ngau nhien
def random_crop(image_path, dir_path, genpath):
    image = cv2.imread(dir_path + image_path)
    
    original_width, original_height = image.shape[1], image.shape[0]
    x_center,y_center = original_height//2, original_width//2
    
    x_left = random.randint(0, x_center//2)
    x_right = random.randint(original_width-x_center//2, original_width)
    
    y_top = random.randint(0, y_center//2)
    y_bottom = random.randint(original_height-y_center//2, original_width)
    
    # crop ra vùng ảnh với kích thước ngẫu nhiên
    cropped_image = image[y_top:y_bottom, x_left:x_right]
    # resize ảnh bằng kích thước ảnh ban đầu 
    cropped_image = cv2.resize(cropped_image, (original_width, original_height))
    cv2.imwrite(genpath + 'crop' + image_path, cropped_image)

# Thay doi do sang 
def change_brightness(image_path, value, dir_path, genpath):
    """
    value: độ sáng thay đổi
    """
    img=cv2.imread(dir_path + image_path)
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)
    v = cv2.add(v,value)
    v[v > 255] = 255
    v[v < 0] = 0
    
    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    
    cv2.imwrite(genpath + 'brightness' + image_path, img)

# Xoay ngau nhien
def rotate_image(image_path, range_angle, dir_path, genpath):
    """
    range_angle: Khoảng góc quay
    """
    image = cv2.imread(dir_path + image_path)
    #lựa chọn ngẫu nhiên góc quay 
    angle = random.randint(-range_angle,range_angle)
    
    img_rot = imutils.rotate(image, angle)
    cv2.imwrite(genpath + 'rotate' + image_path, img_rot)

# List all files in a directory using os.listdir
genpath = './img_in/'
dir_path = './img_out/'
for entry in os.listdir(dir_path):
    if os.path.isfile(os.path.join(dir_path, entry)):
        print(entry)
        value=random.randint(-100,100)  
        add_boder(entry, 100, 400, dir_path, genpath)
        random_crop(entry, dir_path, genpath)
        change_brightness(entry, value, dir_path, genpath)
        rotate_image(entry, 30, dir_path, genpath)

