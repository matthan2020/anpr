import cv2
import imutils
import numpy as np
from os.path import splitext

#xử lý ảnh
def process_img(plate_img):

    # kiểm tra có ảnh vùng biển
    # if (len(place_img)):

    # chuyển đổi đưa ảnh về hệ 8bit
    # plate_img = cv2.convertScaleAbs(place_img, alpha=(255.0))

    # chuyển ảnh sang thang xám và làm mở
    img_gray = cv2.cvtColor(plate_img, cv2.COLOR_BGR2GRAY)
    img_blur = cv2.GaussianBlur(img_gray, (7, 7), 0)

    # Phân ngưỡng ảnh
    binary = cv2.threshold(img_blur, 180, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

    #dãn nở nhằm khử nhiễu và đưa ra ảnh đã được xử lý
    kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    img_thre = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)
    return img_thre

img = cv2.imread('./test/biensoxe1.jpg')
thresh = process_img(img)
cv2.imshow('ss', thresh)
cv2.waitKey(0)