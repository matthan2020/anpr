import cv2
import imutils
import numpy as np
from os.path import splitext
from local_utils import detect_lp
from keras.models import model_from_json

#Load WPOD để detect vùng biển
def load_model(path):
    try:
        path = splitext(path)[0]
        with open('%s.json' % path, 'r') as json_file:
            model_json = json_file.read()
        model = model_from_json(model_json, custom_objects={})
        model.load_weights('%s.h5' % path)
        print("Loading model successfully...")
        return model
    except Exception as e:
        print(e)

#tiền xử lý ảnh
def pre_process_img(img,resize=False):

    # đưa ảnh về thang màu RGB
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    # chuẩn hóa các pixel về dải 0-1
    img = img / 255
    if resize:
        img = cv2.resize(img, (224,224))
    return img

#lầy ra vùng ảnh chứa biển số
def get_plate(img):
    Dmax = 608
    Dmin = 256
    try:
        img_pre_process = pre_process_img(img)
        check = float(max(img_pre_process.shape[:2])) / min(img_pre_process.shape[:2])
        side = int(check * Dmin)
        bound_dim = min(side, Dmax)

        # tìm ra vùng ảnh chứ biển số
        _, place_img, _, cor = detect_lp(wpod_net, img_pre_process, bound_dim, lp_threshold=0.5)
    except:
        place_img = None
        cor = None
    return place_img, cor
#xử lý ảnh
def process_img(place_img):

    # kiểm tra có ảnh vùng biển
    if (len(place_img)):

        # chuyển đổi đưa ảnh về hệ 8bit
        plate_img = cv2.convertScaleAbs(place_img[0], alpha=(255.0))

        # chuyển ảnh sang thang xám và làm mở
        img_gray = cv2.cvtColor(plate_img, cv2.COLOR_BGR2GRAY)
        img_blur = cv2.GaussianBlur(img_gray, (7, 7), 0)

        # Phân ngưỡng ảnh
        binary = cv2.threshold(img_blur, 180, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

        #dãn nở nhằm khử nhiễu và đưa ra ảnh đã được xử lý
        kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        img_thre = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)
    return img_thre


wpod_net = load_model("wpod-net.json")
img = cv2.imread('./test/biensoxe15.jpg')
place_img, cor = get_plate(img)
plate = cv2.cvtColor(place_img[0],cv2.COLOR_RGB2BGR )

# img_thre = process_img(place_img)
# print(place_img)
cv2.imshow('plate', plate)
cv2.waitKey(0)